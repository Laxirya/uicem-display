var gulp = require('gulp'),
    concat = require('gulp-concat'),
    livereload = require('gulp-livereload'),
    spawn = require('child_process').spawn,
    rename = require('gulp-rename'),
    browserify = require('gulp-browserify'),
    less = require('gulp-less'),
    node;

gulp.task('browserify', function () {
  return gulp.src('src/client/scripts/browserify/script.js')
      .pipe(browserify({
        insertGlobals: true
      }))
      .pipe(rename('refine.js'))
      .pipe(gulp.dest('dist/client/scripts'))
      .pipe(livereload());
});

gulp.task('scripts', function() {
    return gulp.src('src/client/scripts/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('dist/client/scripts'))
        .pipe(livereload());
});

gulp.task('styles', function() {
    return gulp.src('src/client/styles/style.less')
        .pipe(less())
        .pipe(gulp.dest('dist/client/css'))
        .pipe(livereload());
});

gulp.task('html', function () {
    return gulp.src('src/client/**/*.html')
        .pipe(gulp.dest('dist/client/'))
        .pipe(livereload());
});

gulp.task('server', function () {
    return gulp.src('src/server/index.js')
        .pipe(gulp.dest('dist/'));
});

gulp.task('restart', function () {
    if (node) node.kill();
    node = spawn('node', ['dist/index.js'], {stdio: 'inherit'});
    node.on('close', function (code) {
        if (code === 8) {
            gulp.log('Error detected, waiting for changes...');
        }
    });
});

gulp.task('watch', function() {
    livereload.listen();

    gulp.start('restart');

    gulp.watch('src/client/scripts/**/*.js', ['scripts', 'browserify']);
    gulp.watch('src/client/styles/**/*.less', ['styles']);
    gulp.watch('src/**/*.html', ['html']);

    gulp.watch('src/server/index.js', ['server', 'restart']);
});

gulp.task('client', ['html', 'scripts', 'browserify', 'styles']);
gulp.task('default', ['server', 'client']);
