var newPadRow = function (item) {
        var row = '<tr>';
        row += '<td><span class="clickable"><u>' + item.pad + '</u></span></td>';
        row += '<td>' + item.nauth + ' utenti</td>';
        row += '<td>'+ item.nlog +' revisioni</td>';
        row += '<td>'+ item.types[1] +' revisioni</td>';
        row += '<td>'+ item.types[3] +' revisioni</td>';
        row += '<td>'+ item.types[4] +' revisioni</td>';
        row += '<td>'+ item.types[5] +' revisioni</td>';
        row += '</tr>';

        return $(row);
    },
    newAuthorRow = function (item) {
            var row = '<tr>';
            row += '<td><span class="clickable"><u>' + item.author + '</u></span></td>';
            row += '<td>' + item.npad + ' pad</td>';
            row += '<td>'+ item.nlog +' revisioni</td>';
            row += '<td>'+ item.types[1] +' revisioni</td>';
            row += '<td>'+ item.types[3] +' revisioni</td>';
            row += '<td>'+ item.types[4] +' revisioni</td>';
            row += '<td>'+ item.types[5] +' revisioni</td>';
            row += '</tr>';

            return $(row);
        },
    newAuthorinPadRow = function (item) {
        var row = '<tr>';
        row += '<td><span class="clickable"><u>' + item.author + '</u></span></td>';
        row += '<td>' + item.nsessions + ' sessioni</td>';
        row += '<td>'+ item.nlog +' revisioni</td>';
        row += '<td>'+ item.logtype[1].ntype +' revisioni</td>';
        row += '<td>'+ item.logtype[3].ntype +' revisioni</td>';
        row += '<td>'+ item.logtype[4].ntype +' revisioni</td>';
        row += '<td>'+ item.logtype[5].ntype +' revisioni</td>';
        row += '</tr>';

        return $(row);
    },
    newPadinAuthorRow = function (item) {
        var myauthinpad = epad[item.padindex].authors[item.authindex],
        row = '<tr>';
        row += '<td><span class="clickable"><u>' + item.pad + '</u></span></td>';
        row += '<td>' + myauthinpad.nsessions + ' sessioni</td>';
        row += '<td>'+ myauthinpad.nlog +' revisioni</td>';
        row += '<td>'+ myauthinpad.logtype[1].ntype +' revisioni</td>';
        row += '<td>'+ myauthinpad.logtype[3].ntype +' revisioni</td>';
        row += '<td>'+ myauthinpad.logtype[4].ntype +' revisioni</td>';
        row += '<td>'+ myauthinpad.logtype[5].ntype +' revisioni</td>';
        row += '</tr>';

        return $(row);
    },
    newSessionRow = function (item, index) {
        var start = new Date(item.sessionstart*1),
            end = new Date(item.sessionend*1),
            tot = item.types[0]+item.types[1]+item.types[2]+item.types[3]+item.types[4]+item.types[5];

        var row = '<tr>';
        row += '<td><span class="clickable"><u>Sessione ' + index + '</u></span></td>';
        row += '<td>' + start.getHours() + ':' +start.getMinutes() + ':' + start.getSeconds() + '(' + start.getDate() + '/' + parseInt(start.getMonth()+1) + '/' + start.getFullYear() + ')</td>';
        row += '<td>' + end.getHours() + ':' + end.getMinutes() + ':' + end.getSeconds() + '(' + end.getDate() + '/' + parseInt(end.getMonth()+1) + '/' + end.getFullYear() + ')</td>';
        row += '<td>'+ tot +' revisioni</td>';
        row += '<td>'+ item.types[1] +' revisioni</td>';
        row += '<td>'+ item.types[3] +' revisioni</td>';
        row += '<td>'+ item.types[4] +' revisioni</td>';
        row += '<td>'+ item.types[5] +' revisioni</td>';
        row += '</tr>';

        return $(row);
    };
