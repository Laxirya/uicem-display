
/*jshint multistr:true */

function init () {
    var padTable = $('<h1 id="page-title-table"></h1><table id="padtable" class="table"></table>'),
        padChart = $('<div id="padchart"></div>'),
        authTable = $('<h1 id="page-title-table"></h1><table id="authtable" class="table"></table>'),
        authChart = $('<div id="authchart"></div>'),
        generalTab = $('<h1 id="page-title"></h1><div id="generalTabcontent"></div>'),
        generalChart = $('<div id="generalChart"></div>');

    $('#main').removeClass('hidden');
    $("#TabList").hide();
    $("#generaltab").empty().append(generalTab).append(generalChart);

    $('#page-title').empty().append('Database');
    var tot = 0;
    epad.forEach(function (currentValue) {
      tot = tot + currentValue.nlog;
    });
    $('#generalTabcontent').empty().append('Numero totale di Pad: ' + epad.length + '<br>Numero totale di Autori: ' + eauth.length + '<br>Numero totale di Revisioni: ' + tot);

    populateMainTab(generalChart);

    $('#general').bind('click', function () {
      $("#TabList").hide();

      $('.nav a[data-target="#generaltab"]').tab('show');

      populateMainTab(generalChart);
    });

    $('#allpads').bind('click', function () {
        $('#table').empty().append(padTable);
        $('#chart').empty().append('<h1 id="page-title-chart"></h1>').append(padChart);

        $("#tabletab").empty().append("Dati");
        $("#charttab").empty().append("Grafici");

        $('.nav a[data-target="#table"]').tab('show');
        $("#TabList").show();

        populateGeneralPadTable(padTable);
        populateGeneralPadChart(padChart);
    });

    $('#allauthors').bind('click', function () {
        $('#table').empty().append(authTable);
        $('#chart').empty().append('<h1 id="page-title-chart"></h1>').append(authChart);

        $("#tabletab").empty().append("Dati");
        $("#charttab").empty().append("Grafici");

        $('.nav a[data-target="#table"]').tab('show');
        $("#TabList").show();

        populateGeneralAuthTable(authTable);
        populateGeneralAuthChart(authChart);
    });

    $("#showbmpad").on("click", function (e) {
      e.preventDefault();
      $("#TabList").hide();
      $('.nav a[data-target="#bookmarkpad"]').tab('show');
    });

    $("#showbmauth").on("click", function (e) {
      e.preventDefault();
      $("#TabList").hide();
      $('.nav a[data-target="#bookmarkauthors"]').tab('show');
    });

    $('#padtable').removeClass('hidden');
}

function populateMainTab(generalChart) {
  var returnItems = generalchart(),
      myChart,
      mainChart = $('<div style="width: 500px"><canvas id="PadBarChart"></canvas></div>'),
      chartType = {
                    idAttribute: 'main-chart',
                    nameAttribute: 'Main',
                    chartAttribute: mainChart
                  };

      generalChart.empty();
      generalChart.append(mainChart);

        myChart = new Chart(mainChart.children(),  {
            type: 'pie',
            responsive: false,
            data:  {
                labels: ["Name Change", "Chat", "Undo/Redo", "Settings"],
                datasets: returnItems,
            }
        });
        mainChart.prepend("<h3>Distribuzione per tipi delle revisioni nel Database</h3>");

}

function populateGeneralPadTable (padTable) {
    var padTableLabel = $('<tr><th>Nome Pad</th><th>Numero Autori</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
    padTable.empty().append(padTableLabel);
    epad.forEach(function(currentValue, index, array){
        var item = newPadRow(currentValue);

        item.bind('click', function () {
            showpad(index);
        });

        padTable.append(item);
    });
    $('#page-title-table').empty().append('Lista di pad nel Database');
}

function populateGeneralPadChart (padChart) {
    var returnItems = padbarchart(),
        myChart,
        nameChangeChart = $('<div style="width: 500px"><canvas id="PadNCChart"></canvas></div>'),
        chatChart = $('<div style="width: 500px"><canvas id="PadCHChart"></canvas></div>'),
        undoRedoChart = $('<div style="width: 500px"><canvas id="PadURChart"></canvas></div>'),
        settingsChart = $('<div style="width: 500px"><canvas id="PadSTTChart"></canvas></div>'),
        chartTypes = [
                {
                    idAttribute: 'name-change-chart',
                    nameAttribute: 'Name change',
                    chartAttribute: nameChangeChart
                },
                {
                    idAttribute: 'chat-chart',
                    nameAttribute: 'Chat',
                    chartAttribute: chatChart
                },
                {
                    idAttribute: 'undo-redo-chart',
                    nameAttribute: 'Undo Redo',
                    chartAttribute: undoRedoChart
                },
                {
                    idAttribute: 'settings-chart',
                    nameAttribute: 'Settings',
                    chartAttribute: settingsChart
                }
            ];

    $('#page-title-chart').empty().append('Grafici del DB');
    padChart.append('<div id="chartSpace"></div>');
    $('#chartSpace').append('<ul class="nav nav-tabs" id="subTab"></ul>');
    $('#chartSpace').append('<div class="tab-content" id="subTabContent"></div>');

    chartTypes.forEach(function (el) {
      var subTab = $('<li><a id="label-' + el.idAttribute + '" data-target="#' + el.idAttribute + '" data-toggle="' + el.idAttribute + '">' + el.nameAttribute + '</a></li>');

      $('#subTabContent').append('<div class="tab-pane" id="' + el.idAttribute + '"></div>');
      $('#subTabContent').children().append(el.chartAttribute);
      $('#subTab').append(subTab);

    });

    $("#subTab").on("click", "a", function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('a[data-target="#name-change-chart"]').parent().addClass('active');
    $('#name-change-chart').addClass('active');

    myChart = new Chart(nameChangeChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.nameChangeSet,
        }
    });
    nameChangeChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti il cambio di Username nel Database</h3>");

    myChart = new Chart(chatChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.chatSet,
        }
    });
    chatChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti la chat nel Database</h3>");

    myChart = new Chart(undoRedoChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.undoRedoSet,
        }
    });
    undoRedoChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti Undo e Redo nel Database</h3>");


    myChart = new Chart(settingsChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.settingsSet,
        }
    });
    settingsChart.prepend("<h3>Distribuzione per Pad dele revisioni riguardanti i Setting nel Database</h3>");

}

function populateGeneralAuthChart (authChart) {
    var returnItems = authcharts(),
        myChart,
        nameChangeChart = $('<div style="width: 500px"><canvas id="PadNCChart"></canvas></div>'),
        chatChart = $('<div style="width: 500px"><canvas id="PadCHChart"></canvas></div>'),
        undoRedoChart = $('<div style="width: 500px"><canvas id="PadURChart"></canvas></div>'),
        settingsChart = $('<div style="width: 500px"><canvas id="PadSTTChart"></canvas></div>'),
        chartTypes = [
                {
                    idAttribute: 'name-change-chart',
                    nameAttribute: 'Name change',
                    chartAttribute: nameChangeChart
                },
                {
                    idAttribute: 'chat-chart',
                    nameAttribute: 'Chat',
                    chartAttribute: chatChart
                },
                {
                    idAttribute: 'undo-redo-chart',
                    nameAttribute: 'Undo Redo',
                    chartAttribute: undoRedoChart
                },
                {
                    idAttribute: 'settings-chart',
                    nameAttribute: 'Settings',
                    chartAttribute: settingsChart
                }
            ];

    $('#page-title-chart').empty().append('Grafici del DB');
    authChart.append('<div id="chartSpace"></div>');
    $('#chartSpace').append('<ul class="nav nav-tabs" id="subTab"></ul>');
    $('#chartSpace').append('<div class="tab-content" id="subTabContent"></div>');

    chartTypes.forEach(function (el) {
      var subTab = $('<li><a id="label-' + el.idAttribute + '" data-target="#' + el.idAttribute + '" data-toggle="' + el.idAttribute + '">' + el.nameAttribute + '</a></li>');

      $('#subTabContent').append('<div class="tab-pane" id="' + el.idAttribute + '"></div>');
      $('#subTabContent').children().append(el.chartAttribute);
      $('#subTab').append(subTab);

    });

    $("#subTab").on("click", "a", function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('a[data-target="#name-change-chart"]').parent().addClass('active');
    $('#name-change-chart').addClass('active');

    myChart = new Chart(nameChangeChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.nameChangeSet,
        }
    });
    nameChangeChart.prepend("<h3>Distribuzione per Autori delle revisioni riguardanti il cambio di Username nel Database</h3>");

    myChart = new Chart(chatChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.chatSet,
        }
    });
    chatChart.prepend("<h3>Distribuzione per Autori delle revisioni riguardanti la chat nel Database</h3>");

    myChart = new Chart(undoRedoChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.undoRedoSet,
        }
    });
    undoRedoChart.prepend("<h3>Distribuzione per Autori delle revisioni riguardanti Undo e Redo nel Database</h3>");


    myChart = new Chart(settingsChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.settingsSet,
        }
    });
    settingsChart.prepend("<h3>Distribuzione per Autori dele revisioni riguardanti i Setting nel Database</h3>");

}

function populateGeneralAuthTable (authTable) {
    var authTableLabel = $('<tr><th>Nome Autore</th><th>Numero Pad</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
    authTable.empty().append(authTableLabel);
    eauth.forEach(function(currentValue, index, array){
        var item = newAuthorRow(currentValue);

        item.bind('click', function () {
            showauthor(index);
        });

        authTable.append(item);
    });
    $('#page-title-table').empty().append('Lista di autori nel Database');
}

function showpad(padindex){
    var singlePadTable = $('<table id="usertable" class="table"></table>'),
        singlePadChart = $('<div id="singlepadchart"></div>'),
        back = '<div id="back" class="bookmark-button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Generale</div>',
        bookmarkButton = '<div id="addbookmark" align="right" class="bookmark-button"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-bookmark-o fa-inverse fa-stack-1x"></i></span> Aggiungi ai Bookmark</div>';

    $("#tabletab").empty().append("Dati");
    $("#charttab").empty().append("Grafici");

    $('#table').empty().append(back + '<h1 id="page-title-table"></h1>' + bookmarkButton).append(singlePadTable);
    $('#chart').empty().append('<h1 id="page-title-chart"></h1>').append(singlePadChart);


    populateSinglePadTable(singlePadTable, padindex);
    populateSinglePadChart(singlePadChart, padindex);
}

function showauthor(authorindex){
    var singleAuthTable = $('<table id="padtable" class="table"></table>'),
        singleAuthChart = $('<div id="singleauthchart"></div>'),
        back = '<div id="back" class="bookmark-button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Generale</div>',
        bookmarkButton = '<div id="addbookmark" align="right" class="bookmark-button"><span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-bookmark-o fa-inverse fa-stack-1x"></i></span> Aggiungi ai Bookmark</div>';

    $("#tabletab").empty().append("Dati");
    $("#charttab").empty().append("Grafici");

    $('#table').empty().append(back + '<h1 id="page-title-table"></h1>' + bookmarkButton).append(singleAuthTable);
    $('#chart').empty().append('<h1 id="page-title-chart"></h1>').append(singleAuthChart);


    populateSingleAuthTable (singleAuthTable, authorindex);
    populateSingleAuthorChart(singleAuthChart, authorindex);
}

function populateSinglePadTable (singlePadTable, padindex) {
  var mypad = epad[padindex];
  var singlePadTableLabel = $('<tr><th>Nome Autore</th><th>Numero Sessioni</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
  singlePadTable.empty().append(singlePadTableLabel);

  mypad.authors.forEach(function(currentValue, index, array){
      var itemAuthor = newAuthorinPadRow(currentValue);

      itemAuthor.bind('click', function () {
        showauthorinpad(padindex, index);
      });

      singlePadTable.append(itemAuthor);
  });
  $('#page-title-table').empty().append('Lista di Autori nel pad ' + mypad.pad);
  var addbookmark = $( "#addbookmark" );
  var isbookmarked = 0;
  bookmarks.pad.forEach(function(bm){
    if (bm == padindex) {
      isbookmarked=1;
    }
  });
  if (!isbookmarked) {
    addpadbookmark(addbookmark, padindex);
  } else {
    deletepadbookmark(addbookmark, padindex);
  }
  var back = $( "#back" );
  back.empty().append('<i class="fa fa-arrow-left" aria-hidden="true"></i> Generale');
  $("#back").bind("click", function (){
    var padTable = $('<h1 id="page-title-table"></h1><table id="padtable" class="table"></table>'),
        padChart = $('<div id="padchart"></div>');

    $('#table').empty().append(padTable);
    $('#chart').empty().append('<h1 id="page-title-chart"></h1>').append(padChart);

    $('.nav a[data-target="#table"]').tab('show');

    populateGeneralPadTable(padTable);
    populateGeneralPadChart(padChart);
  });

}

function populateSingleAuthTable (singleAuthTable, authindex) {
  var myauth = eauth[authindex];
  var singleAuthTableLabel = $('<tr><th>Nome Pad</th><th>Numero Sessioni</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
  singleAuthTable.empty().append(singleAuthTableLabel);

  myauth.pads.forEach(function(currentValue, index, array){

      var itemAuthor = newPadinAuthorRow(currentValue);

      itemAuthor.bind('click', function () {

        showauthorinpad(currentValue.padindex, currentValue.authindex);
      });

      singleAuthTable.append(itemAuthor);
  });
  $('#page-title-table').empty().append('Lista di Pad dell\'Autore ' + myauth.author);
  var addbookmark = $( "#addbookmark" );
  var isbookmarked = 0;
  bookmarks.autori.forEach(function(bm){
    if (bm == authindex) {
      isbookmarked=1;
    }
  });
  if (!isbookmarked) {
    addauthbookmark(addbookmark, authindex);
  } else {
    deleteauthbookmark(addbookmark, authindex);
  }

  var back = $( "#back" );
  back.empty().append('<i class="fa fa-arrow-left" aria-hidden="true"></i> Generale');
  $("#back").bind("click", function (){
    var authTable = $('<h1 id="page-title-table"></h1><table id="authtable" class="table"></table>'),
        authChart = $('<div id="authchart"></div>');

    $('#table').empty().append(authTable);
    $('#chart').empty().append('<h1 id="page-title-chart"></h1>').append(authChart);

    $('.nav a[data-target="#table"]').tab('show');

    populateGeneralAuthTable(authTable);
    populateGeneralAuthChart(authChart);
  });
}

function addpadbookmark (addbookmark, padindex) {
  addbookmark.empty().append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-bookmark-o fa-inverse fa-stack-1x"></i></span> Aggiungi ai Bookmark');
  $('#addbookmark').bind("click", function(){
    socket.emit('add_to_bookmark', {type: 0, index: epad[padindex].pad});
    deletepadbookmark(addbookmark, padindex);
  });
}

function addauthbookmark (addbookmark, authindex) {
  addbookmark.empty().append('<span class="fa-stack fa-lg"><i class="fa fa-circle fa-stack-2x"></i><i class="fa fa-bookmark-o fa-inverse fa-stack-1x"></i></span> Aggiungi ai Bookmark');
  $('#addbookmark').bind("click", function(){
    socket.emit('add_to_bookmark', {type: 1, index: eauth[authindex].authorid});
    deleteauthbookmark(addbookmark, authindex);
  });
}

function deletepadbookmark (addbookmark, padindex) {
  addbookmark.empty().append('<span class="fa-stack fa-lg"><i class="active fa fa-circle fa-stack-2x"></i><i class="fa fa-bookmark fa-inverse fa-stack-1x"></i></span> Rimuovi dai Bookmark');
  $('#addbookmark').bind("click", function(){
    socket.emit('delete_bookmark', {type: 0, index: epad[padindex].pad});
    addpadbookmark(addbookmark, padindex);
  });
}

function deleteauthbookmark (addbookmark, authindex) {
  addbookmark.empty().append('<span class="fa-stack fa-lg"><i class="active fa fa-circle fa-stack-2x"></i><i class="fa fa-bookmark fa-inverse fa-stack-1x"></i></span> Rimuovi dai Bookmark');
  $('#addbookmark').bind("click", function(){
    socket.emit('delete_bookmark', {type: 1, index: eauth[authindex].authorid});
    addauthbookmark(addbookmark, authindex);
  });
}

function populateSinglePadChart(chart, padindex) {
    var returnItems = authbarchart(padindex),
        myChart,
        mainChart = $('<div style="width: 500px"><canvas id="PadBarChart"></canvas></div>'),
        nameChangeChart = $('<div style="width: 500px"><canvas id="PadNCChart"></canvas></div>'),
        chatChart = $('<div style="width: 500px"><canvas id="PadCHChart"></canvas></div>'),
        undoRedoChart = $('<div style="width: 500px"><canvas id="PadURChart"></canvas></div>'),
        settingsChart = $('<div style="width: 500px"><canvas id="PadSTTChart"></canvas></div>'),
        chartTypes = [
                {
                    idAttribute: 'main-chart',
                    nameAttribute: 'Main',
                    chartAttribute: mainChart
                },
                {
                    idAttribute: 'name-change-chart',
                    nameAttribute: 'Name change',
                    chartAttribute: nameChangeChart
                },
                {
                    idAttribute: 'chat-chart',
                    nameAttribute: 'Chat',
                    chartAttribute: chatChart
                },
                {
                    idAttribute: 'undo-redo-chart',
                    nameAttribute: 'Undo Redo',
                    chartAttribute: undoRedoChart
                },
                {
                    idAttribute: 'settings-chart',
                    nameAttribute: 'Settings',
                    chartAttribute: settingsChart
                }
            ];

    $('#page-title-chart').empty().append('Grafici del pad ' + epad[padindex].pad);
    chart.append('<div id="chartSpace"></div>');
    $('#chartSpace').append('<ul class="nav nav-tabs" id="subTab"></ul>');
    $('#chartSpace').append('<div class="tab-content" id="subTabContent"></div>');


    chartTypes.forEach(function (el) {
        var subTab = $('<li><a id="label-' + el.idAttribute + '" data-target="#' + el.idAttribute + '" data-toggle="' + el.idAttribute + '">' + el.nameAttribute + '</a></li>');

        $('#subTabContent').append('<div class="tab-pane" id="' + el.idAttribute + '"></div>');
        $('#subTabContent').children().append(el.chartAttribute);
        $('#subTab').append(subTab);

    });

    $("#subTab").on("click", "a", function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('a[data-target="#main-chart"]').parent().addClass('active');
    $('#main-chart').addClass('active');

    myChart = new Chart(mainChart.children(),  {
        type: 'pie',
        responsive: false,
        data:  {
            labels: ["Name Change", "Chat", "Undo/Redo", "Settings"],
            datasets: returnItems.dataset,
        }
    });
    mainChart.prepend("<h3>Distribuzione per tipi delle revisioni riguardanti il Pad " + epad[padindex].pad + "</h3>");

    // nameChangeChart.prepend("<h1>Distribuzione per Pad dei log riguardanti il cambio di Username</h1>");
    myChart = new Chart(nameChangeChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.nameChangeSet,
        }
    });
    nameChangeChart.prepend("<h3>Distribuzione per Autore delle revisioni riguardanti il cambio di Username nel Pad " + epad[padindex].pad + "</h3>");

    // chatChart.prepend("<h1>Distribuzione per Pad dei log riguardanti la chat</h1>");
    myChart = new Chart(chatChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.chatSet,
        }
    });
    chatChart.prepend("<h3>Distribuzione per Autore delle revisioni riguardanti la chat nel Pad " + epad[padindex].pad + "</h3>");

    // undoRedoChart.prepend("<h1>Distribuzione per Pad dei log riguardanti Undo e Redo</h1>");
    myChart = new Chart(undoRedoChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.undoRedoSet,
        }
    });
    undoRedoChart.prepend("<h3>Distribuzione per Autore delle revisioni riguardanti Undo e Redo nel Pad " + epad[padindex].pad + "</h3>");

    // settingsChart.prepend("<h1>Distribuzione per Pad dei log riguardanti i Setting</h1>");
    myChart = new Chart(settingsChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.settingsSet,
        }
    });
    settingsChart.prepend("<h3>Distribuzione per Autore delle revisioni riguardanti i Setting nel Pad " + epad[padindex].pad + "</h3>");

}

function populateSingleAuthorChart(chart, authindex) {
    var returnItems = singleauthbarchart(authindex),
        myChart,
        mainChart = $('<div style="width: 500px"><canvas id="PadBarChart"></canvas></div>'),
        nameChangeChart = $('<div style="width: 500px"><canvas id="PadNCChart"></canvas></div>'),
        chatChart = $('<div style="width: 500px"><canvas id="PadCHChart"></canvas></div>'),
        undoRedoChart = $('<div style="width: 500px"><canvas id="PadURChart"></canvas></div>'),
        settingsChart = $('<div style="width: 500px"><canvas id="PadSTTChart"></canvas></div>'),
        chartTypes = [
                {
                    idAttribute: 'main-chart',
                    nameAttribute: 'Main',
                    chartAttribute: mainChart
                },
                {
                    idAttribute: 'name-change-chart',
                    nameAttribute: 'Name change',
                    chartAttribute: nameChangeChart
                },
                {
                    idAttribute: 'chat-chart',
                    nameAttribute: 'Chat',
                    chartAttribute: chatChart
                },
                {
                    idAttribute: 'undo-redo-chart',
                    nameAttribute: 'Undo Redo',
                    chartAttribute: undoRedoChart
                },
                {
                    idAttribute: 'settings-chart',
                    nameAttribute: 'Settings',
                    chartAttribute: settingsChart
                }
            ];

    $('#page-title-chart').empty().append('Grafici dell\'autore ' + eauth[authindex].author);
    chart.append('<div id="chartSpace"></div>');
    $('#chartSpace').append('<ul class="nav nav-tabs" id="subTab"></ul>');
    $('#chartSpace').append('<div class="tab-content" id="subTabContent"></div>');


    chartTypes.forEach(function (el) {
        var subTab = $('<li><a id="label-' + el.idAttribute + '" data-target="#' + el.idAttribute + '" data-toggle="' + el.idAttribute + '">' + el.nameAttribute + '</a></li>');

        $('#subTabContent').append('<div class="tab-pane" id="' + el.idAttribute + '"></div>');
        $('#subTabContent').children().append(el.chartAttribute);
        $('#subTab').append(subTab);

    });

    $("#subTab").on("click", "a", function (e) {
        e.preventDefault();
        $(this).tab('show');
    });

    $('a[data-target="#main-chart"]').parent().addClass('active');
    $('#main-chart').addClass('active');

    myChart = new Chart(mainChart.children(),  {
        type: 'pie',
        responsive: false,
        data:  {
            labels: ["Name Change", "Chat", "Undo/Redo", "Settings"],
            datasets: returnItems.dataset,
        }
    });
    mainChart.prepend("<h3>Distribuzione per tipi delle revisioni riguardanti l\'autore " + eauth[authindex].author+"</h3>");

    // nameChangeChart.prepend("<h1>Distribuzione per Pad dei log riguardanti il cambio di Username</h1>");
    myChart = new Chart(nameChangeChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.nameChangeSet,
        }
    });
    nameChangeChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti il cambio di Username l\'autore " + eauth[authindex].author+"</h3>");

    // chatChart.prepend("<h1>Distribuzione per Pad dei log riguardanti la chat</h1>");
    myChart = new Chart(chatChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.chatSet,
        }
    });
    chatChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti la chat del\'autore " + eauth[authindex].author+"</h3>");

    // undoRedoChart.prepend("<h1>Distribuzione per Pad dei log riguardanti Undo e Redo</h1>");
    myChart = new Chart(undoRedoChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.undoRedoSet,
        }
    });
    undoRedoChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti Undo e Redo del\'autore " + eauth[authindex].author+"</h3>");

    // settingsChart.prepend("<h1>Distribuzione per Pad dei log riguardanti i Setting</h1>");
    myChart = new Chart(settingsChart.children(), {
        type: 'pie',
        responsive: false,
        data:  {
            labels: returnItems.pielabels,
            datasets: returnItems.settingsSet,
        }
    });
    settingsChart.prepend("<h3>Distribuzione per Pad delle revisioni riguardanti i Setting del\'autore " + eauth[authindex].author+"</h3>");

}

function showauthorinpad(padindex, authindex){
  var backtopad = '<div id="backtopad" class="bookmark-button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Pad: ' + epad[padindex].pad +'</div>',
      backtoauth = '<div id="backtoauth" class="bookmark-button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Autore: ' + epad[padindex].authors[authindex].author +'</div>',
      authorTable = $('<h1 id="page-title-table"></h1><table id="authortable" class="table"></table>'),
      authorChart = $('<h1 id="page-title-chart"></h1><div id="authorchart"></div>');

  $("#tabletab").empty().append("Dati");
  $("#charttab").empty().append("Grafici");

  $('#table').empty().append(backtopad).append(backtoauth).append(authorTable);
  $('#chart').empty().append(authorChart);


  populateAuthorTable (authorTable, padindex, authindex);
  populateAuthorChart(authorChart, padindex, authindex);

}

function populateAuthorTable (authorTable, padindex, authindex) {
  var myauth = epad[padindex].authors[authindex];
  var authorTableLabel = $('<tr><th>Numero Sessione</th><th>Data di inizio</th><th>Data di Fine</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
  authorTable.empty().append(authorTableLabel);

    myauth.sessions.forEach(function(currentValue, index, array){
        var sessionItem = newSessionRow(currentValue, index);

        sessionItem.bind('click', function () {

          chargeinfo(padindex, authindex, index);
        });
        authorTable.append(sessionItem);
    });
    $('#page-title-table').empty().append('Lista di Sessioni dell\'Autore ' + myauth.author + ' nel pad ' + epad[padindex].pad);

    var backtopad = $( "#backtopad" );
    backtopad.empty().append('<i class="fa fa-arrow-left" aria-hidden="true"></i> Pad: ' + epad[padindex].pad);
    $("#backtopad").bind("click", function (){
      showpad(padindex);
    });

    var backtoauth = $( "#backtoauth" );
    backtoauth.empty().append('<i class="fa fa-arrow-left" aria-hidden="true"></i> Autore: ' + myauth.author);
    $("#backtoauth").bind("click", function (){
      showauthor(myauth.authindex);
    });
}

function populateAuthorChart(authorTable, padindex, authindex) {
  var returnItems = authorbarchart(padindex, authindex),
      myChart,
      mainChart = $('<div style="width: 500px"><canvas id="PadBarChart"></canvas></div>');

  authorTable.append(mainChart);

  myChart = new Chart(mainChart.children(),  {
      type: 'pie',
      responsive: false,
      data:  {
          labels: ["Name Change", "Chat", "Undo/Redo", "Settings"],
          datasets: returnItems,
      }
  });
  mainChart.prepend("<h3>Distribuzione per Tipo delle revisioni dell'autore "+ epad[padindex].authors[authindex].author +" nel pad "+ epad[padindex].pad +"</h3>");

  $('#page-title-chart').empty().append('Grafici dell\'Autore ' + epad[padindex].authors[authindex].author);
}

function chargeinfo(padindex, authindex, sessionindex) {
    var myauth = epad[padindex].authors[authindex];
    var mysession = myauth.sessions[sessionindex];
    var sstart = new Date(mysession.sessionstart*1);
    var ssend = new Date(mysession.sessionend*1);

    $("#tabletab").empty().append("Revisioni");
    $("#charttab").empty().append("Scroll");

    $('#table').empty().append('<div id="back" class="bookmark-button"><i class="fa fa-arrow-left" aria-hidden="true"></i> Autore: ' + myauth.author + ' + Pad: ' + epad[padindex].pad + '</div>').append("<h1>Revisioni dell'autore " + myauth.author + " del pad " + epad[padindex].pad + " nella sessione numero " + sessionindex + " iniziata il giorno " + sstart.getDate() + "/" + parseInt(sstart.getMonth()+1) + "/" + sstart.getFullYear() + "</h1>");
    $('#chart').empty().append("<h1>Movimenti di scrolling della sessione numero "+sessionindex +" iniziata il giorno "+ sstart.getDate() + "/" + parseInt(sstart.getMonth()+1) + "/" + sstart.getFullYear() + " alle ore " + sstart.getHours() + ":" + sstart.getMinutes() + ":" + sstart.getSeconds() + ":" + sstart.getMilliseconds() + " e conclusa il giorno "+ ssend.getDate() + "/" + parseInt(ssend.getMonth()+1) + "/" + ssend.getFullYear() + " alle ore " + ssend.getHours() + ":" + ssend.getMinutes() + ":" + ssend.getSeconds() + ":" + ssend.getMilliseconds() + "</h1>");

    var nc = [];
    myauth.logtype[1].log.forEach(function(currentValue, index, array) {
        if (currentValue.timestamp > mysession.sessionstart && currentValue.timestamp < mysession.sessionend) {
            date = new Date(currentValue.timestamp*1);
            nc.push("Username of user "+ currentValue.userid +" changed from "+ currentValue.prename +" to "+ currentValue.postname +"<br>"+ date +"<br><br>");
        }
    });
    $('#table').append("<h3>Name Changes</h3>"+nc);

    var mv = [];
    var nl = [];
    myauth.logtype[2].log.forEach(function(currentValue, index, array){
        if (currentValue.timestamp > mysession.sessionstart && currentValue.timestamp < mysession.sessionend) {
            var movement = false;
            date = new Date(currentValue.timestamp*1);
            nl.push(currentValue);
            if (array[index-1]) {
                if (parseInt(currentValue.lenght) > parseInt(array[index-1].lenght)) {
                    movement = "down from line "+array[index-1].lenght+" to line "+currentValue.lenght;
                } else if (currentValue.lenght < array[index-1].lenght) {
                    movement = "up from line "+array[index-1].lenght+" to line "+currentValue.lenght;
                }
            } else {
                movement = "to line "+currentValue.lenght;
            }
            if (movement) mv.push("User "+currentValue.userid+" scrolled "+movement+"<br>"+ date+"<br><br>");
        }
    });
    $('#table').append("<h3>Scrolling Movement</h3>"+mv);
    scrollingchart({data: nl, session: mysession, pad: padindex});

    var ch = [];
    myauth.logtype[3].log.forEach(function(currentValue, index, array) {
        if (currentValue.timestamp > mysession.sessionstart && currentValue.timestamp < mysession.sessionend) {
            date = new Date(currentValue.timestamp*1);
            ch.push("User "+ currentValue.userid +": chat is now "+ currentValue.action +"<br>"+ date +"<br><br>");
        }
    });
    $('#table').append("<h3>Chat Logs</h3>"+ch);

    var urt = [];
    myauth.logtype[4].log.forEach(function(currentValue, index, array) {
        if (currentValue.timestamp > mysession.sessionstart && currentValue.timestamp < mysession.sessionend) {
            date = new Date(currentValue.timestamp*1);
            urt.push("User "+ currentValue.userid +" clicked button: "+ currentValue.azione +"<br>"+ date +"<br><br>");
        }
    });
    $('#table').append("<h3>Undo and Redos</h3>"+urt);

    var stt = [];
    myauth.logtype[5].log.forEach(function(currentValue, index, array) {
        if (currentValue.timestamp > mysession.sessionstart && currentValue.timestamp < mysession.sessionend) {
            date = new Date(currentValue.timestamp*1);
            if (currentValue.setting == "selected" ) {
              stt.push("User "+ currentValue.userid +" opened settings menù<br>"+ date +"<br><br>");
            } else if (currentValue.setting == "deselected") {
              stt.push("User "+ currentValue.userid +" closed settings menù<br>"+ date +"<br><br>");
            } else {
              stt.push("User "+ currentValue.userid +" changed his settings: "+ currentValue.setting +"<br>"+ date +"<br><br>");
            }
        }
    });
    $('#table').append("<h3>Setting Changes</h3>"+stt);

    var back = $( "#back" );
    back.empty().append('<i class="fa fa-arrow-left" aria-hidden="true"></i> Autore: ' + myauth.author + ' + Pad: ' + epad[padindex].pad);
    $("#back").bind("click", function (){
      showauthorinpad(padindex, authindex);
    });

}
