showPadBookmarks = function(){
  var titlepadbookmarks = $('<h1 id="title-bm-table"></h1>');
  var padbookmarks = $('<table id="padbookmarks" class="table"></table>');
  $('#bookmarkpad').empty().append(titlepadbookmarks).append(padbookmarks);
  var bmPadTableLabel = $('<tr><th>Nome Pad</th><th>Numero Autori</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
  padbookmarks.empty().append(bmPadTableLabel);
  bookmarks.pad.forEach(function(pad){
    var item = newPadRow(epad[pad]);
    item.bind('click', function () {
        showpad(pad);
        $('.nav a[data-target="#table"]').tab('show');
        $("#TabList").show();
    });

    padbookmarks.append(item);
  });
  $('#title-bm-table').empty().append('Lista di Pad nei Bookmarks');
};

showAuthorsBookmarks = function(){
  var titleauthorsbookmarks = $('<h1 id="title-bm-authtable"></h1>');
  var authorsbookmarks = $('<table id="authbmtable" class="table"></table>');
  $('#bookmarkauthors').empty().append(titleauthorsbookmarks).append(authorsbookmarks);
  var bmAuthTableLabel = $('<tr><th>Nome Autore</th><th>Numero Pad</th><th>Numero Revisioni Totali</th><th>Numero Revisioni sul Cambio Nome</th><th>Numero Revisioni sulla Chat</th><th>Numero Revisioni su Undo/Redo</th><th>Numero Revisoni sulle Opzioni</th></tr>');
  authorsbookmarks.empty().append(bmAuthTableLabel);
  bookmarks.autori.forEach(function(auth){
    var item = newAuthorRow(eauth[auth]);
    item.bind('click', function () {
        showauthor(auth);
        $('.nav a[data-target="#table"]').tab('show');
        $("#TabList").show();
    });

    authorsbookmarks.append(item);
  });
  $('#title-bm-authtable').empty().append('Lista di Autori nei Bookmarks');
};
