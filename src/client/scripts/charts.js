function generalchart() {
  var dataset=[{
    data: [0, 0, 0, 0],
    backgroundColor: [typescolors[0].fillColor, typescolors[1].fillColor, typescolors[2].fillColor, typescolors[3].fillColor],
    hoverBackgroundColor: [typescolors[0].highlightFill, typescolors[1].highlightFill, typescolors[2].highlightFill, typescolors[3].highlightFill]
  }];
  epad.forEach(function (currentValue, index, array){
    dataset[0].data[0] = dataset[0].data[0]+currentValue.types[1];
    dataset[0].data[1] = dataset[0].data[1]+currentValue.types[3];
    dataset[0].data[2] = dataset[0].data[2]+currentValue.types[4];
    dataset[0].data[3] = dataset[0].data[3]+currentValue.types[5];
  });
  return (dataset);
}

function padbarchart() {
  var pielabels=[],
    nameChangeSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
    }],
    chatSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
    }],
    undoRedoSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
    }],
    settingsSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
      }];

  epad.forEach(function (currentValue, index, array){
    var colors = authcolors[index%(authcolors.length)];
    pielabels.push(currentValue.pad);

    nameChangeSet[0].data.push(currentValue.types[1]);
    nameChangeSet[0].backgroundColor.push(colors.fillColor);
    nameChangeSet[0].hoverBackgroundColor.push(colors.highlightFill);

    chatSet[0].data.push(currentValue.types[3]);
    chatSet[0].backgroundColor.push(colors.fillColor);
    chatSet[0].hoverBackgroundColor.push(colors.highlightFill);

    undoRedoSet[0].data.push(currentValue.types[4]);
    undoRedoSet[0].backgroundColor.push(colors.fillColor);
    undoRedoSet[0].hoverBackgroundColor.push(colors.highlightFill);

    settingsSet[0].data.push(currentValue.types[5]);
    settingsSet[0].backgroundColor.push(colors.fillColor);
    settingsSet[0].hoverBackgroundColor.push(colors.highlightFill);
  });


  return {'nameChangeSet': nameChangeSet, 'chatSet': chatSet, 'undoRedoSet': undoRedoSet, 'settingsSet': settingsSet, 'pielabels': pielabels};
}

function authcharts() {
  var pielabels=[],
    nameChangeSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
    }],
    chatSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
    }],
    undoRedoSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
    }],
    settingsSet=[{
        data: [],
        backgroundColor: [],
        hoverBackgroundColor: []
      }];

  eauth.forEach(function (currentValue, index, array){
    var colors = authcolors[index%(authcolors.length)];
    pielabels.push(currentValue.author);

    nameChangeSet[0].data.push(currentValue.types[1]);
    nameChangeSet[0].backgroundColor.push(colors.fillColor);
    nameChangeSet[0].hoverBackgroundColor.push(colors.highlightFill);

    chatSet[0].data.push(currentValue.types[3]);
    chatSet[0].backgroundColor.push(colors.fillColor);
    chatSet[0].hoverBackgroundColor.push(colors.highlightFill);

    undoRedoSet[0].data.push(currentValue.types[4]);
    undoRedoSet[0].backgroundColor.push(colors.fillColor);
    undoRedoSet[0].hoverBackgroundColor.push(colors.highlightFill);

    settingsSet[0].data.push(currentValue.types[5]);
    settingsSet[0].backgroundColor.push(colors.fillColor);
    settingsSet[0].hoverBackgroundColor.push(colors.highlightFill);
  });


  return {'nameChangeSet': nameChangeSet, 'chatSet': chatSet, 'undoRedoSet': undoRedoSet, 'settingsSet': settingsSet, 'pielabels': pielabels};
}

function authbarchart(padindex) {
  var dataset=[{
    data: [0, 0, 0, 0],
    backgroundColor: [typescolors[0].fillColor, typescolors[1].fillColor, typescolors[2].fillColor, typescolors[3].fillColor],
    hoverBackgroundColor: [typescolors[0].highlightFill, typescolors[1].highlightFill, typescolors[2].highlightFill, typescolors[3].highlightFill]
  }];
  var pielabels=[];
  var nameChangeSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  var chatSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  var undoRedoSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  var settingsSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  epad[padindex].authors.forEach(function (currentValue, index, array){
    var colors = authcolors[index%(authcolors.length)];
    pielabels.push(currentValue.author);

    nameChangeSet[0].data.push(currentValue.logtype[1].ntype);
    nameChangeSet[0].backgroundColor.push(colors.fillColor);
    nameChangeSet[0].hoverBackgroundColor.push(colors.highlightFill);

    chatSet[0].data.push(currentValue.logtype[3].ntype);
    chatSet[0].backgroundColor.push(colors.fillColor);
    chatSet[0].hoverBackgroundColor.push(colors.highlightFill);

    undoRedoSet[0].data.push(currentValue.logtype[4].ntype);
    undoRedoSet[0].backgroundColor.push(colors.fillColor);
    undoRedoSet[0].hoverBackgroundColor.push(colors.highlightFill);

    settingsSet[0].data.push(currentValue.logtype[5].ntype);
    settingsSet[0].backgroundColor.push(colors.fillColor);
    settingsSet[0].hoverBackgroundColor.push(colors.highlightFill);

    dataset[0].data[0] = dataset[0].data[0]+currentValue.logtype[1].ntype;
    dataset[0].data[1] = dataset[0].data[1]+currentValue.logtype[3].ntype;
    dataset[0].data[2] = dataset[0].data[2]+currentValue.logtype[4].ntype;
    dataset[0].data[3] = dataset[0].data[3]+currentValue.logtype[5].ntype;
  });


  return {'dataset': dataset, 'nameChangeSet': nameChangeSet, 'chatSet': chatSet, 'undoRedoSet': undoRedoSet, 'settingsSet': settingsSet, 'pielabels': pielabels};
}

function singleauthbarchart(authindex) {
  var myauth = eauth[authindex],
  dataset=[{
    data: [myauth.types[1], myauth.types[3], myauth.types[4], myauth.types[5]],
    backgroundColor: [typescolors[0].fillColor, typescolors[1].fillColor, typescolors[2].fillColor, typescolors[3].fillColor],
    hoverBackgroundColor: [typescolors[0].highlightFill, typescolors[1].highlightFill, typescolors[2].highlightFill, typescolors[3].highlightFill]
  }];
  var pielabels=[];
  var nameChangeSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  var chatSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  var undoRedoSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  var settingsSet=[{
    data: [],
    backgroundColor: [],
    hoverBackgroundColor: []
  }];
  eauth[authindex].pads.forEach(function (authvalue, index, array){
    var colors = authcolors[index%(authcolors.length)],
        currentValue = epad[authvalue.padindex].authors[authvalue.authindex];
    pielabels.push(currentValue.author);

    nameChangeSet[0].data.push(currentValue.logtype[1].ntype);
    nameChangeSet[0].backgroundColor.push(colors.fillColor);
    nameChangeSet[0].hoverBackgroundColor.push(colors.highlightFill);

    chatSet[0].data.push(currentValue.logtype[3].ntype);
    chatSet[0].backgroundColor.push(colors.fillColor);
    chatSet[0].hoverBackgroundColor.push(colors.highlightFill);

    undoRedoSet[0].data.push(currentValue.logtype[4].ntype);
    undoRedoSet[0].backgroundColor.push(colors.fillColor);
    undoRedoSet[0].hoverBackgroundColor.push(colors.highlightFill);

    settingsSet[0].data.push(currentValue.logtype[5].ntype);
    settingsSet[0].backgroundColor.push(colors.fillColor);
    settingsSet[0].hoverBackgroundColor.push(colors.highlightFill);
  });


  return {'dataset': dataset, 'nameChangeSet': nameChangeSet, 'chatSet': chatSet, 'undoRedoSet': undoRedoSet, 'settingsSet': settingsSet, 'pielabels': pielabels};
}

function authorbarchart(padindex, authindex){
  var dataset=[{
    data: [0, 0, 0, 0],
    backgroundColor: [typescolors[0].fillColor, typescolors[1].fillColor, typescolors[2].fillColor, typescolors[3].fillColor],
    hoverBackgroundColor: [typescolors[0].highlightFill, typescolors[1].highlightFill, typescolors[2].highlightFill, typescolors[3].highlightFill]
  }];
  epad[padindex].authors[authindex].sessions.forEach(function (currentValue, index, array){
    dataset[0].data[0] = dataset[0].data[0]+currentValue.types[1];
    dataset[0].data[1] = dataset[0].data[1]+currentValue.types[3];
    dataset[0].data[2] = dataset[0].data[2]+currentValue.types[4];
    dataset[0].data[3] = dataset[0].data[3]+currentValue.types[5];
  });
  return (dataset);
}

function scrollingchart(msg){
    var stats = msg.data;
    var mydata=[];
    var scrollingdata=[];
    var startdate = new Date(msg.session.sessionstart*1);
    scrollingdata.push([startdate, 1]);
    stats.forEach(function (currentValue, index, array) {
        var date = new Date(currentValue.timestamp*1);
        scrollingdata.push([date, currentValue.lenght]);
    });
    scrollingdata.push([date, scrollingdata[scrollingdata.length-1][1]]);
    $('#chart').append("<div id=\"ScrollingChart\" style=\"width:1000px; height:400px\"></div>");

    var options = {
        xaxis: {
            mode: 'time',
            timeformat: '%H:%M:%S'
        }
    };

    $.plot($("#ScrollingChart"), [scrollingdata], options);
}
