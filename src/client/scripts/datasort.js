var epad = [],
    eauth = [],
    eptypes = [0, 0, 0, 0, 0, 0],
    bookmarks;

socket.on('list_of_data', function(dataset) {
  var data = dataset.data,
      names = dataset.authors;
  data.forEach(function(dataentry){
    var datatype;
    if (!dataentry.azione.indexOf("Session")) {
        datatype=0;
    } else if (!dataentry.azione.indexOf("name change")) {
        datatype=1;
    } else if (!dataentry.azione.indexOf("pad scrolling: line")) {
        datatype=2;
    } else if (!dataentry.azione.indexOf("chat")) {
        datatype=3;
    } else if (dataentry.azione=="undo" || dataentry.azione=="redo") {
        datatype=4;
    } else if (!dataentry.azione.indexOf("settings:")) {
        datatype=5;
    }
    eptypes[datatype]++;
    var existentpad = false,
        existentauthor = false;
    epad.forEach(function(padentry, padentryindex){
      if (dataentry.pad == padentry.pad) {
        existentpad = true;
        padentry.nlog++;
        padentry.types[datatype]++;
        var existentauthorinpad=false;
        padentry.authors.forEach(function (authentry, authentryindex){
          if (authentry.authorid == dataentry.utente){
            existentauthorinpad=true;
            authentry.nlog++;
            insertlog(padentryindex, authentryindex, dataentry, datatype);
          }
        });
        if (!existentauthorinpad){
          newauthor(padentryindex, dataentry, datatype, names);
        }
      }
    });
    if (!existentpad) {
      newpad(dataentry, datatype, names);
    }
    eauth.forEach(function(authentry, authentryindex) {
      if (dataentry.utente == authentry.authorid){
        existentauthor=true;
        authentry.nlog++;
        authentry.types[datatype]++;
        var existentpadinauthor=false;
        authentry.pads.forEach(function (padentry, padentryindex){
          if (padentry.pad == dataentry.pad) {
            existentpadinauthor=true;
          }
        });
        if (!existentpadinauthor) {
          authentry.npad++;
          authentry.pads.push({pad: dataentry.pad});
        }
      }
    });
    if (!existentauthor) {
      var name = dataentry.utente;
      pads = [];
      types = [0, 0, 0, 0, 0, 0];
      types[datatype]++;
      names.forEach(function(currentValue){
        if (dataentry.utente == currentValue.index) {
          name = currentValue.name;
        }
      });
      eauth.push({author: name, authorid: dataentry.utente, npad: 1, nlog: 1, types: types, pads: pads});
      eauth[eauth.length-1].pads.push({pad: dataentry.pad});
    }
  });

  epad.forEach(function (padentry, padentryindex){
    padentry.authors.forEach(function (authentry, authentryindex){
      authentry.logtype.forEach(function (typeentry, logtypeindex){
        typeentry.log.forEach(function (logentry){
          authentry.sessions.forEach(function (sessionentry){
            var ts = new Date(logentry.timestamp*1);
            var start = new Date(sessionentry.sessionstart*1);
            var end = new Date(sessionentry.sessionend*1);
            if (ts > start && ts < end) {
              sessionentry.types[logtypeindex]++;}
          });
        });
      });
      eauth.forEach(function (eauthentry, eauthentryindex){
        eauthentry.pads.forEach(function (epadentry){
          if (authentry.authorid == eauthentry.authorid && padentry.pad == epadentry.pad) {
            epadentry.padindex = padentryindex;
            epadentry.authindex = authentryindex;
            authentry.authindex = eauthentryindex;
          }
        });
      });
    });
  });
  init();
  socket.emit('getbookmarks');
});

function newpad(dataentry, datatype, names) {
  authors = [];
  types = [0, 0, 0, 0, 0, 0];
  types[datatype]++;
  var padindex = epad.push({pad: dataentry.pad, nauth: 0, nlog: 1, types: types, authors: authors});
  padindex--;
  newauthor(padindex, dataentry, datatype, names);
}

function newauthor(padindex, dataentry, datatype, names) {
  var logtype = [
    {type: "Session", ntype: 0, isopen: false, log: []},
    {type: "Name Change", ntype: 0, log: []},
    {type: "Scrolling", ntype: 0, log: []},
    {type: "Chat", ntype: 0, log: []},
    {type: "Undo/Redo", ntype: 0, log: []},
    {type: "Settings", ntype: 0, log: []}
  ],
      name,
      authindex,
      sessions=[];
  names.forEach(function(currentValue){
    if (dataentry.utente == currentValue.index) {
      name = currentValue.name;
    }
  });
  epad[padindex].nauth++;
  authindex = epad[padindex].authors.push({author: name, authorid: dataentry.utente, nlog: 1, nsessions: 0, logtype: logtype, sessions: sessions});
  authindex--;
  insertlog(padindex, authindex, dataentry, datatype);
}

function insertlog (padindex, authindex, dataentry, datatype) {
  var date = dataentry.timestamp;
  var types = [0, 0, 0, 0, 0, 0];
  if (datatype===0) {
      var session = dataentry.azione.split(" ")[1];
      var sessiontype = epad[padindex].authors[authindex].logtype[datatype];
      sessiontype.ntype++;
      sessiontype.log.push({userid: dataentry.utente, session: session, timestamp: date});
      if (!sessiontype.isopen && session=="Start") {
        sessiontype.laststart=date;
        sessiontype.isopen=true;
      } else if (sessiontype.isopen && session=="End") {
        epad[padindex].authors[authindex].sessions.push({sessionstart: sessiontype.laststart, sessionend: date, types: types});
        epad[padindex].authors[authindex].nsessions++;
        sessiontype.isopen=false;
      }
  } else if (datatype===1) {
      var prename = dataentry.azione.split("\"")[1];
      var postname = dataentry.azione.split("\"")[3];
      epad[padindex].authors[authindex].logtype[datatype].ntype++;
      epad[padindex].authors[authindex].logtype[datatype].log.push({userid: dataentry.utente, prename: prename, postname: postname, timestamp: date});
  } else if (datatype===2) {
      var lenght = dataentry.azione.split(" ")[3];
      epad[padindex].authors[authindex].logtype[datatype].ntype++;
      epad[padindex].authors[authindex].logtype[datatype].log.push({userid: dataentry.utente, lenght: lenght, timestamp: date});
  } else if (datatype===3) {
      var action;
      if (!dataentry.azione.indexOf("chat icon clicked")) {
          action = "open";
      } else if (!dataentry.azione.indexOf("chat titlecross clicked")) {
          action = "closed";
      } else if (!dataentry.azione.indexOf("chat stick to screen button checked")) {
          action = "open";
      } else {
          action = "scrolling";
      }
      epad[padindex].authors[authindex].logtype[datatype].ntype++;
      epad[padindex].authors[authindex].logtype[datatype].log.push({userid: dataentry.utente, action: action, timestamp: date});
  } else if (datatype===4) {
      epad[padindex].authors[authindex].logtype[datatype].ntype++;
      epad[padindex].authors[authindex].logtype[datatype].log.push({userid: dataentry.utente, azione: dataentry.azione, timestamp: date});
  } else if (datatype===5) {
      var setting = dataentry.azione.split(": ")[1];
      epad[padindex].authors[authindex].logtype[datatype].ntype++;
      epad[padindex].authors[authindex].logtype[datatype].log.push({userid: dataentry.utente, setting: setting, timestamp: date});
  }
}




socket.on('bookmarkssort', function(data){
  bookmarks= {pad: [], autori: []};
  data.forEach(function(data){
    if (data.type===0){
      epad.forEach(function (pad, padindex) {
        if (pad.pad == data.value) {
          bookmarks.pad.push(padindex);
        }
      });
    } else {
      eauth.forEach(function (auth, authindex) {
        if (auth.authorid == data.value) {
          bookmarks.autori.push(authindex);
        }
      });
    }
  });
  showPadBookmarks();
  showAuthorsBookmarks();
});
