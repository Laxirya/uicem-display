/* FIRST DISPLAY + CREDENTIALS FORM */
$('#main').addClass('hidden');
$("#credentialsForm").bind("submit", function(e){
    var credentials = {
        host: $("#chost").val(),
        user: $("#cusername").val(),
        password: $("#cpassword").val(),
        database: $("#cdatabase").val()
    };

    //per evitare che la pagina si ricarichi al submit
    e.preventDefault();

    socket.emit("sendcredentials", credentials);
});
