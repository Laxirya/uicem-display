var mysql = require('mysql'),
    connection,
    express = require('express'),
    app = express(),
    http = require('http').Server(app),
    io = require('socket.io')(http),
    user;

app.get('/', function(req, res){
  res.sendFile(__dirname + '/client/index.html');
});

app.use('/scripts', express.static(__dirname + '/client/scripts'));
app.use('/css', express.static(__dirname + '/client/css'));

app.use('/jquery', express.static(__dirname + '/jquery'));

app.use('/flot', express.static(__dirname + '/../node_modules/flot/'));

app.use('/chart.js', express.static(__dirname + '/../node_modules/chart.js/'));

io.on('connection', function(socket){
  socket.on('sendcredentials', function(msg){
    user=msg.user;
    connection = mysql.createConnection(msg);
    connection.query('CREATE TABLE IF NOT EXISTS bookmarks ( ID INT NOT NULL AUTO_INCREMENT PRIMARY KEY , user VARCHAR(50) NOT NULL , type INT NOT NULL , value VARCHAR(50) NOT NULL )');
    var data=[],
        authorsnames=[];
    connection.query('SELECT * from monitor ORDER BY chiave', function(err, rows, fields) {
      if (!err) {
        rows.forEach(function (currentValue, index, array){
          data.push(currentValue);
        });
      } else console.log('Error while performing Query. ' + err);
      connection.query('SELECT * from store', function(err, rows, fields) {
        if (!err) {
          rows.forEach(function (currentValue, index, array){
            if (!currentValue.key.indexOf("globalAuthor:")){
              var id = currentValue.key.split(":")[1];
              var name = currentValue.value.split(",")[1].split(":")[1];
              if(name == "null" || name == "\"\""){
                name = id;
              } else {
                name=name.split("\"")[1];
              }
              authorsnames.push({name: name, index: id});
            }
          });
        } else console.log('Error while performing Query.');
        io.emit('list_of_data', {data: data, authors: authorsnames});
      });
    });
  });
  socket.on('add_to_bookmark', function(msg){
    connection.query('INSERT INTO bookmarks (user, type, value) VALUES (\''+user+'\', '+msg.type+', \''+msg.index+'\')');
    refreshbookmarks();
  });
  socket.on('delete_bookmark', function(msg){
    connection.query('DELETE FROM bookmarks WHERE type = '+msg.type+' and value = \''+msg.index+'\'');
    refreshbookmarks();
  });

  socket.on('getbookmarks', function(){
    refreshbookmarks();
  });
});

refreshbookmarks = function () {
  var data=[];
  connection.query('SELECT * FROM bookmarks WHERE user = \''+ user +'\'', function(err, rows, fields) {
    if (!err) {
      rows.forEach(function (currentValue, index, array){
        data.push(currentValue);
      });
    } else console.log('Error while performing Query.');
    io.emit('bookmarkssort', data);
  });
};


http.listen(8001, function(){
  console.log('Server running at http://127.0.0.1:8001/');
});
